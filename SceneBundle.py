#Python.NET
from Deadline.Plugins import *
from Deadline.Scripting import *

import re

loggerName = 'SCENE_BUNDLE'

def list2cmdline(seq):
    """
    Translate a sequence of arguments into a command line
    string, using the same rules as the MS C runtime:

    1) Arguments are delimited by white space, which is either a
       space or a tab.

    2) A string surrounded by double quotation marks is
       interpreted as a single argument, regardless of white space
       contained within.  A quoted string can be embedded in an
       argument.

    3) A double quotation mark preceded by a backslash is
       interpreted as a literal double quotation mark.

    4) Backslashes are interpreted literally, unless they
       immediately precede a double quotation mark.

    5) If backslashes immediately precede a double quotation mark,
       every pair of backslashes is interpreted as a literal
       backslash.  If the number of backslashes is odd, the last
       backslash escapes the next double quotation mark as
       described in rule 3.
    """

    # See
    # http://msdn.microsoft.com/en-us/library/17w5ykft.aspx
    # or search http://msdn.microsoft.com for
    # "Parsing C++ Command-Line Arguments"
    result = []
    needquote = False
    for arg in seq:
        bs_buf = []

        # Add a space to separate this argument from the others
        if result:
            result.append(' ')

        needquote = (" " in arg) or ("\t" in arg) or not arg
        if needquote:
            result.append('"')

        for c in arg:
            if c == '\\':
                # Don't know if we need to double yet.
                bs_buf.append(c)
            elif c == '"':
                # Double backslashes.
                result.append('\\' * len(bs_buf)*2)
                bs_buf = []
                result.append('\\"')
            else:
                # Normal char
                if bs_buf:
                    result.extend(bs_buf)
                    bs_buf = []
                result.append(c)

        # Add remaining backslashes, if any.
        if bs_buf:
            result.extend(bs_buf)

        if needquote:
            result.extend(bs_buf)
            result.append('"')

    return ''.join(result)

def GetDeadlinePlugin():
    return SceneBundlePlugin()

class OnError(object):
    IGNORE    = 0b0000
    LOG       = 0b0001
    RAISE     = 0b0010
    ASK       = 0b0100
    EXIT      = 0b1000
    THROW     = RAISE
    QUIT      = EXIT
    LOG_RAISE = LOG | RAISE
    LOG_ASK   = LOG | ASK
    LOG_EXIT  = LOG | EXIT
    ALL = LOG | RAISE | ASK | EXIT

class SceneBundlePlugin(DeadlinePlugin):
    ProcessName = "SceneBundle"
    Process = None

    bundle_exp = (r'\s*%s\s*:'%loggerName + r'\s*(%s)\s*:' +
            r'\s*(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}?)\s*:')
            # + r'(.*)')
    sentinel_exp = r'\s*:\s*(END_%s)' % loggerName

    non_bundle_exp = r'(?!\s*%s:\s*)(.*)' % loggerName
    question_exp = bundle_exp%'INFO' + r'\s*Question\s*:\s*(.*)' + sentinel_exp
    error_exp = ( bundle_exp%'ERROR' + r'\s*(.*)\s*')
    error_sentinel_exp = ( bundle_exp%'ERROR' + r'\s*(.*)\s*' + sentinel_exp +
            '\s*')
    warning_sentinel_exp = ( bundle_exp%'WARNING' + r'\s*(.*)\s*(' +
            sentinel_exp + ')?\s*')
    warning_exp = ( bundle_exp%'WARNING' + r'\s*(.*)\s*' )
    progress_exp = ( bundle_exp%'INFO' + '\s*Progress\s*:' +
            '\s*([^\s]*)\s*:\s*(\d+)\s*of\s*(\d+)\s*' + sentinel_exp)
    process_exp = (bundle_exp%'INFO' + r'\s*Process\s*:\s*([^\s]*)\s*' +
            sentinel_exp + '\s*')
    status_exp = ( bundle_exp%'INFO' + r'\s*Status\s*:\s*([^\s]*)\s*:' +
            r'\s*(.*)\s*' + sentinel_exp + '\s*')
    done_exp = ( bundle_exp%'INFO' + r'\s*DONE\s*' + sentinel_exp + '\s*')

    process = None
    status = None
    context = None
    context_msg = None
    errors = None
    warnings = None

    def __init__(self):
        # init function
        self.InitializeProcessEvent += self.InitializeProcess
        self.RenderArgumentEvent += self.RenderArgument
        self.RenderExecutableEvent += self.RenderExecutable

    def InitializeProcess(self):
        # setting up from attributes and stdout listeners
        self.SingleFramesOnly = True
        self.PluginType = PluginType.Simple
        self.errors = []
        self.warnings = []
        self._process = 0

        self.UseProcessTree = True
        self.StdoutHandling = True

        self.AddStdoutHandlerEvent(self.question_exp).HandleEvent += (
                self.handleQuestion )
        self.AddStdoutHandlerEvent(self.progress_exp).HandleEvent += (
                self.handleProgress )
        self.AddStdoutHandlerEvent(self.error_exp).HandleEvent += (
                self.handleError )
        self.AddStdoutHandlerEvent(self.error_sentinel_exp).HandleEvent += (
                self.handleErrorSentinel )
        self.AddStdoutHandlerEvent(self.warning_exp).HandleEvent += (
                self.handleWarning )
        self.AddStdoutHandlerEvent(self.warning_sentinel_exp).HandleEvent += (
                self.handleWarningSentinel )
        self.AddStdoutHandlerEvent(self.process_exp).HandleEvent += (
                self.handleProcess )
        self.AddStdoutHandlerEvent(self.status_exp).HandleEvent += (
                self.handleStatus )
        self.AddStdoutHandlerEvent(self.done_exp).HandleEvent += (
                self.handleDone )
        self.AddStdoutHandlerEvent(self.non_bundle_exp).HandleEvent += (
                self.handleNonBundleLine )

    def suspendJob(self):
        job = GetJob()
        LogInfo('Question encountered: SUSPENDING %s!' % job.JobId)
        ClientUtils.ExecuteCommand([ 'SuspendJob', job.JobId ])

    def handleNonBundleLine(self):
        msg = self.GetRegexMatch(0)
        if self.context is not None:
            match = re.search(self.sentinel_exp, msg)
            if match:
                msg = re.sub(self.sentinel_exp, '', msg)
                self.context_msg += msg
                self.endContext()
            else:
                self.context_msg += msg
        # self.SuppressThisLine()

    def handleQuestion(self):
        self.suspendJob()
        FailRender('Question encountered ... Fail!')

    def handleProgress(self):
        process = self.GetRegexMatch(3)
        val = int( self.GetRegexMatch(4) )
        maxx = int( self.GetRegexMatch(5) )
        if val == 0:
            SetProgress(0)
        else:
            SetProgress(100 * val / maxx)
        LogInfo('%s :: Progress: %s ==> %s of %s' % (loggerName, process, val,
            maxx) )
        self.SuppressThisLine()

    def handleError(self):
        msg = self.GetRegexMatch(3)
        self.startContext(context='error', msg=msg)
        self.SuppressThisLine()

    def handleErrorSentinel(self):
        msg = self.GetRegexMatch(3)
        self.error(msg)
        self.SuppressThisLine()

    def handleWarning(self):
        msg = self.GetRegexMatch(3)
        self.startContext(context='warning', msg=msg)
        self.SuppressThisLine()

    def handleWarningSentinel(self):
        msg = self.GetRegexMatch(3)
        self.warning(msg)
        self.SuppressThisLine()

    def handleProcess(self):
        self._process = self.GetRegexMatch(3)
        LogInfo('%s :: Starting Process: %s'%( loggerName, self._process ))
        SetStatusMessage(self._process)
        self.SuppressThisLine()

    def handleStatus(self):
        proc = self.GetRegexMatch(3)
        msg = self.GetRegexMatch(4)
        SetStatusMessage( msg )
        LogInfo('%s :: Status: %s: %s' %(loggerName, proc, msg))
        self.SuppressThisLine()

    def handleDone(self):
        LogInfo('%s :: Done Signal Recieved ... Exiting With Success' %
                loggerName)
        try:
            ExitWithSuccess()
        except:
            pass
        self.SuppressThisLine()

    def RenderExecutable(self):
        self.version = GetPluginInfoEntry('Version')
        self.build = GetPluginInfoEntry('Build')

        executable = ""
        mayapyList = GetConfigEntry('MayaPy' + self.version)
        if self.build == '32bit':
            executable = SearchFileListFor32Bit(mayapyList)
        elif self.build == '64bit':
            executable = SearchFileListFor64Bit(mayapyList)
        else:
            executable = SearchFileList(mayapyList)
        if executable == "":
            FailRender('%s Build of Maya %s was not Found'%(build, version))
        return executable

    def getSceneBundleData(self):
        self.sceneBundleLocation = GetConfigEntry( 'SceneBundleLocation' )
        self.sceneFile = GetPluginInfoEntry( 'SceneFile' )
        self.tempPath = GetConfigEntry( 'TempPath' )
        self.name = GetPluginInfoEntry( 'Name' )
        self.keepReferences = GetBooleanPluginInfoEntryWithDefault(
                'KeepReferences', False)
        self.archive = GetBooleanPluginInfoEntryWithDefault( 'Archive', False )
        self.delete = GetBooleanPluginInfoEntryWithDefault( 'Delete', True )
        self.deadline = GetBooleanPluginInfoEntryWithDefault('Deadline', True)
        self.exceptions = GetPluginInfoEntryWithDefault( 'Exceptions', '' )
        self.onError = GetIntegerPluginInfoEntryWithDefault( 'OnError', 1 )
        self.project = GetPluginInfoEntryWithDefault( 'Project', '' )
        self.episode = GetPluginInfoEntryWithDefault( 'Episode', '' )
        self.sequence = GetPluginInfoEntryWithDefault( 'Sequence', '' )
        self.shot = GetPluginInfoEntryWithDefault( 'Shot', '' )

    def RenderArgument(self):
        self.getSceneBundleData()

        args = []
        args.append(self.sceneBundleLocation)
        args.append(self.sceneFile)
        args.extend(['-tp', self.tempPath])
        args.extend(['-n', self.name])
        if self.keepReferences:
            args.append('-r')
        if self.archive:
            args.append('-a')
        if self.delete:
            args.append('-x')
        if self.deadline:
            args.append('-p')
            args.extend(['-p', self.project])
            args.extend(['-ep', self.episode])
            args.extend(['-s', self.sequence])
            args.extend(['-t', self.shot])

        for exc in self.exceptions.split(';'):
            args.extend(['-e', exc])
        args.extend(['-err', str( self.onError )])

        return list2cmdline(args)

    def startContext(self, context='error', msg=None):
        self.context = context
        if msg is None:
            self.context_msg = ''
        else:
            self.context_msg = msg

    def endContext(self):
        if self.context is not None:
            if self.context == 'error':
                self.error(self.context_msg)
            elif self.context == 'warning':
                self.warning(self.context_msg)
            self.context = None
            self.context_msg = None

    def error(self, msg):
        self.errors.append(msg)
        LogWarning('%s :: SceneBundle Error: %s' % ( loggerName, msg ))

    def warning(self):
        self.warning.append(msg)
        LogWarning('%s :: SceneBundle Warning: %s' % ( loggerName, msg ))

